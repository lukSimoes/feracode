import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
    openedModal: any;
    error: any = false;
    formRegister: any = {
        nome: '',
        email: '',
        phone: '',
        password: ''
    };

    formLogin: any = {
        email: '',
        password: ''
    };




    constructor( private router: Router) {

    }

    ngOnInit() {
    }

    open(target, contentModal? , callback?) {

        document.documentElement.classList.add('is-scroll-disabled');
        this.openedModal = document.querySelector(target);
        this.openedModal.dataset.opened = 'true';
        callback && callback(this.openedModal);

        // Add youtube iframe video src or video src
        if (this.openedModal.dataset.video) {
            if (this.openedModal.dataset.video.includes('youtube') ) {
                const vParam = this.openedModal.dataset.video.split('v=');
                const videoId = vParam[1].includes('&') ? vParam[1].substr(0, vParam[1].indexOf('&')) : vParam[1];
                const embed = 'https://www.youtube.com/embed/' + videoId + '?rel=0&amp;controls=1&amp;showinfo=0&amp;autoplay=1';
                this.openedModal.querySelector('iframe').src = embed;
            } else {
                this.openedModal.querySelector('video').src = this.openedModal.dataset.video;
            }
        }
        // Add iframe src
        if (this.openedModal.dataset.iframe) {
            this.openedModal.querySelector('iframe').src = this.openedModal.dataset.iframe;
        }
    }

    close(event?, callback?) {
        if (event) {
            event.preventDefault(), event.stopPropagation();
            if (!event.target.dataset.hasOwnProperty('modalClose')) { return false; }
        }

        document.documentElement.classList.remove('is-scroll-disabled');
        this.openedModal.dataset.opened = 'false';

        // Remove video iframe url or iframe src
        if ( this.openedModal.dataset.iframe ) {
            this.openedModal.querySelector('iframe').src = '';
        }

        // Remove video iframe src or video src
        if ( this.openedModal.dataset.video ) {
            if ( this.openedModal.dataset.video.includes('youtube') ) {
                this.openedModal.querySelector('iframe').src = '';
            } else {
                this.openedModal.querySelector('video').pause();
            }
        }

        callback && callback(this.openedModal);
    }
    change(target, event?) {
        event && (event.preventDefault(), event.stopPropagation());
        this.openedModal.dataset.opened = 'false';
        this.openedModal = document.querySelector(target);
        this.openedModal.dataset.opened = 'true';
    }

    alert(element, text, title) {
        this.open(element, false, ( element ) => {
            text && (element.querySelector('.modal--content').innerHTML = text);
            title && (element.querySelector('.modal--title').innerHTML = title);
        });
    }


    loginf() {
        const users = JSON.parse(localStorage.getItem('users'));
        if (users) {
            const retorno = users.find( obj => {
                if (obj) {
                    return   obj.email === this.formLogin.email && obj.password === this.formLogin.password ? obj : false;
                }
                return false;
            } );

            if (retorno) {
               localStorage.setItem('currentUser', JSON.stringify(retorno));
               this.router.navigateByUrl('/logged', { skipLocationChange: true });
               return;
            }

        }
        this.error = 'user not registered or incorrect password';
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.router.navigateByUrl('/', { skipLocationChange: true });

    }

    register() {
        let users = JSON.parse(localStorage.getItem('users'));
        if (users) {
            const retorno = users.find( obj => {
                if (obj) {
                    return   obj.email === this.formRegister.email ? obj : false;
                }
                return false;
            } );

            if (retorno) {
                this.error = 'user already registered';
                console.log(this.error);
                return;
            }
        } else {
            users = [];
        }
        const date = new Date();
        this.formRegister.date = date.getDate() + '/' + ( date.getMonth() + 1) + '/' + date.getFullYear();
        users.push(this.formRegister);
        localStorage.setItem('users', JSON.stringify(users));
        this.formRegister = {};
        this.error = false;
        this.close();
        this.alert('#alert', 'successfully registered.', 'Congratulation');
    }

}
