import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {ModalComponent} from '../modal/modal.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
 changeBackground: any = false;
 user: any;
 @ViewChild(ModalComponent)  modal: ModalComponent;
  constructor() {

  }
    @HostListener('window:scroll', ['$event']) onScrollEvent($event) {
      this.changeBackground = window.pageYOffset > 90 ? true : false;
      console.log(this.changeBackground);

    }
    ngOnInit() {
        this.changeBackground = window.pageYOffset > 90 ? true : false;
        this.user = JSON.parse(localStorage.getItem('currentUser'));
       // this.modal.open('#modaFormulario');
    }

    logout() {
        this.modal.logout();
        this.user = false;
    }

}
