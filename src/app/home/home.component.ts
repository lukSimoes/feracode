import { Component, OnInit } from '@angular/core';
import MeucaroselInstance from '../../assets/js/meuCarousel.js';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  instanceMeu: any;
  constructor() { }

  ngOnInit() {
    this.instanceMeu = new MeucaroselInstance({
        selector: 'carousel',
        slidesPerView: 4,
        spaceBetween: 20,
        loop: true,
        timePerPage: 4000,
        stopOnMouseHover: true
    });
    console.log( this.instanceMeu);

  }

}
