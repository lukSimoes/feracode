import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {HomeComponent} from './home/home.component';
import {LoggedComponent} from './logged/logged.component';

const  ROUTES: Routes = [

    {path: '', component: HomeComponent, } ,
    {path: 'logged', component: LoggedComponent, } ,

];

export const routing: ModuleWithProviders = RouterModule.forRoot(ROUTES);
