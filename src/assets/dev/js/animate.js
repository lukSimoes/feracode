var Animation = function() {
    var elements = document.querySelectorAll('[data-animation]');

    // Define a dobra superior, inferior e laterais da tela
    var windowOffset = 6;
    var windowTop = 0;
    var windowBottom = window.innerHeight - ( window.innerHeight / windowOffset );
    var windowLeft = 0;
    var windowRight = window.innerWidth;

    function start(element) {
        // Seta os atributos customizados
        if ( element.dataset.animationDelay ) element.style.animationDelay = element.dataset.animationDelay;
        if ( element.dataset.animationDuration ) element.style.animationDuration = element.dataset.animationDuration;

        // Inicia a animacao setando a classe da animacao
        if ( element.dataset.animation ) element.classList.add(element.dataset.animation);
        // Seta o elemento como animado
        element.dataset.animated = 'true';
    }

    function isElementOnScreen(element) {
        // Obtem o boundingbox do elemento
        var elementRect = element.getBoundingClientRect();
        var elementTop = elementRect.top + parseInt(element.dataset.animationOffset) || elementRect.top;
        var elementBottom = elementRect.bottom - parseInt(element.dataset.animationOffset) || elementRect.bottom;
        var elementLeft = elementRect.left;
        var elementRight = elementRect.right;

        // Verifica se o elemento esta na tela
        return ( elementTop <= windowBottom )
            && ( elementBottom >= windowTop )
            && ( elementLeft <= windowRight )
            && ( elementRight >= windowLeft );
    }

    // Verifica se o elemento esta na tela
    function _checkElementOnScreen() {
        for ( var i = 0, len = elements.length; i < len; i++ ) {
            // Passa para o proximo loop se o elemento ja estiver animado
            if ( elements[i].dataset.animated ) continue;
            if ( isElementOnScreen( elements[i] ) ) start( elements[i] );
        }
    }

    // Inicia os eventos
    window.addEventListener('load', _checkElementOnScreen, false);
    window.addEventListener('scroll', _checkElementOnScreen, false);
    window.addEventListener('resize', _checkElementOnScreen, false);

    // Retorna funcoes publicas
    return {
        start: start,
        isElementOnScreen: isElementOnScreen,
    };
}();
