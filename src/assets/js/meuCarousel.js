export default class MeuCarousel {
    constructor(config) {
        this.config = config;
        this.init();
    }
    init() {

        // Select the carousel you'll need to manipulate and the buttons you'll add events to
        const carousel = document.querySelector("[data-target='" + this.config.selector + "']");
        const card = carousel.querySelector("[data-target='card']");
        const leftButton = document.querySelector("[data-action='slideLeft']");
        const rightButton = document.querySelector("[data-action='slideRight']");
        let interval ='';
        let activePage = 1;
        let timeToNext =  this.config.timePerPage;
        let stopSlide = false;

        // Prepare to limit the direction in which the carousel can slide,
        // and to control how much the carousel advances by each time.
        // In order to slide the carousel so that only three cards are perfectly visible each time,
        // you need to know the carousel width, and the margin placed on a given card in the carousel
        const carouselWidth = carousel.offsetWidth;
        const cardStyle = card.currentStyle || window.getComputedStyle(card)
        const cardMarginRight = this.config.spaceBetween;

        const cardSize = this.config.slidesPerView > 1 ? (carouselWidth / this.config.slidesPerView) - cardMarginRight : (carouselWidth / this.config.slidesPerView);
        const allItens = carousel.querySelectorAll("[data-target='card']");
        allItens.forEach((item) => {
            item.style.width = `${cardSize}px`;


        if (this.config.slidesPerView > 1) {
            item.style.marginRight = `${cardMarginRight/2}px`;
            item.style.marginLeft = `${cardMarginRight/2}px`
        }


    })
        // Count the number of total cards you have
        const cardCount = carousel.querySelectorAll("[data-target='card']").length;
        const totalPage = Math.ceil(cardCount / this.config.slidesPerView);
        // Define an offset property to dynamically update by clicking the button controls
        // as well as a maxX property so the carousel knows when to stop at the upper limit
        let offset = 0;
        const maxX = -((cardCount / totalPage) * carouselWidth +
            (cardMarginRight * (cardCount / totalPage)) -
            carouselWidth - cardMarginRight);

        let prev = () => {
            if (offset !== 0 && offset !== 20 && activePage > 1 && stopSlide == false) {
                activePage -= 1;
                offset += (carouselWidth + cardMarginRight) - 20;
                carousel.removeAttribute('style');
                carousel.style.transform = `translateX(${offset}px)`;
            }
        }

        let next = () => {
            if ((offset !== maxX && activePage < totalPage && stopSlide == false)) {
                activePage += 1;
                offset -= (carouselWidth + cardMarginRight) - 20;
                carousel.style.transition = 'all 1s ease';
                carousel.style.transform = `translateX(${offset}px)`;


            } else if ((this.config.slidesPerView == 1 && activePage < totalPage && stopSlide == false)) {
                activePage += 1;
                offset -= carouselWidth;
                carousel.style.transform = `translateX(${offset}px)`;
            } else if (activePage == totalPage && stopSlide == false) {
                clearInterval(interval);
                timeToNext = (this.config.timePerPage/2);
                infinite();

                carousel.querySelectorAll("[data-target='card']").forEach((item, index) => {

                    if(index < this.config.slidesPerView) {
                    carousel.append(item);
                }


            })
                prev();



            }
        }

        // Add the click events


        carousel.addEventListener("mouseover", () => {
            stopSlide = true;
    })

        carousel.addEventListener("mouseout", () => {
            stopSlide = false;
    })



        let infinite = () => {
            if(this.config.loop){
                interval =  setInterval(() => {
                    next();
            }, timeToNext);
            }

        }

        infinite();


    }
}
